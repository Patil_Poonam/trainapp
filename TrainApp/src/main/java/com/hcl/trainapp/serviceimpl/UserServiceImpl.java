package com.hcl.trainapp.serviceimpl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;


import com.hcl.trainapp.dto.UserDto;
import com.hcl.trainapp.exception.UserIdNotFoundException;
import com.hcl.trainapp.model.User;
import com.hcl.trainapp.repository.UserRepository;
import com.hcl.trainapp.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;
	int Id = (int) (Math.random()*Math.pow(10, 3)); 

	/*
	 * @Override public String userRegister(User user) { user.setUserId("1");
	 * 
	 * int userId = user.getUserId(); userRepository.save(user);
	 * 
	 * 
	 * 
	 * return userId; }
	 */
	@Override
	public String userLogin(int userId, String password) {
		User user = userRepository.findByUserIdAndPassword(userId, password);
		if (user == null) {
		throw new UserIdNotFoundException(userId + " NotFound");
		}
		return "User has successfully logged in";
	}

	@Override
	public boolean saveUserDetails(UserDto userDto) {
		User user = new User();
		BeanUtils.copyProperties(userDto, user);
		User userPerst = userRepository.save(user);
		if(ObjectUtils.isEmpty(userPerst)) return false;
		return true;
	}
	

	
}
