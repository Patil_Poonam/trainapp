package com.hcl.trainapp.service;

import com.hcl.trainapp.dto.UserDto;
import com.hcl.trainapp.model.User;

public interface UserService {

	//String userRegister(User user);

	String userLogin(int userId, String password);

	boolean saveUserDetails(UserDto userDto);

}
