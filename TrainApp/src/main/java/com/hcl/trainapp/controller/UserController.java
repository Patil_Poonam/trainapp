package com.hcl.trainapp.controller;

import org.modelmapper.ModelMapper;
//import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.hcl.trainapp.dto.UserDto;
import com.hcl.trainapp.model.User;
import com.hcl.trainapp.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	ModelMapper mapper = new ModelMapper();

	@PostMapping("/users")
	public String userRegistration(@RequestBody UserDto userDto) {

		boolean isSaved = userService.saveUserDetails(userDto);
        if(isSaved) return "Registration Successful";
		return "Registration failed";
	}
	

	@GetMapping("/user/login")
	String userLogin(@RequestParam(value = "userId") int userId, @RequestParam(value = "password") String password) {

		return userService.userLogin(userId, password);

	}
}